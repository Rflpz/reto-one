//
//  RLViewController.m
//  Reto-Uno
//
//  Created by dcl17 on 04/02/14.
//  Copyright (c) 2014 dcl17. All rights reserved.
//

#import "RLViewController.h"
#import "RLViewController.m"

@interface RLViewController ()
@property (weak, nonatomic)IBOutlet UITextField *data;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *myContrasint;
@property (weak,nonatomic) IBOutlet NSLayoutConstraint *myConstraint2;
@end

@implementation RLViewController
- (IBAction)touchButton:(id)sender {
    if(![self.data.text isEqualToString:@""]){
        self.label.text = self.data.text;
        [UIView animateWithDuration:0.3
                         animations:^{
                            self.myContrasint.constant = -200;
                             self.myConstraint2.constant = -800;
                             self.label.alpha = 1.0;
                             self.data.alpha = 0;
                             self.button.alpha = 0;
                             [self.view layoutIfNeeded];
                         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Ingresa texto en el campo" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.data resignFirstResponder];
    [self.nextResponder touchesEnded:touches withEvent:event];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.label.alpha = 0;
}
@end
